FROM ubuntu:22.04

RUN useradd -ms /bin/bash gitlab-runner

# Install dependencies
RUN apt update -y && apt upgrade -y && \
    apt install -y python3.10 python3-pip vim && \
    rm -rf /var/lib/apt/lists/*

USER gitlab-runner

WORKDIR /home/gitlab-runner

ENV PATH="/home/gitlab-runner/.local/bin:${PATH}"